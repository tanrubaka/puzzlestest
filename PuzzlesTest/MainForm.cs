﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using PuzzlesTest.Domains;

namespace PuzzlesTest
{
    public partial class MainForm : Form
    {
        private List<Triangle> _triangles;
        private List<Container> _containers;
        private string _filePath;
        private Bitmap _source;
        private bool _move = false;
        private bool _gameOn = false;
        private bool _autoSize = false;
        private double _curScale = 1;
        private readonly int _epsilon = Configuration.AppConfig.Epsilon;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Init()
        {
            if (string.IsNullOrEmpty(_filePath))
            {
                MessageBox.Show(Properties.Resources.select_file, Properties.Resources.file_not_selected);
                return;
            }
            _curScale = 1;
            _source = new Bitmap(Image.FromFile(_filePath));
            _triangles = Utils.Puzzle.InitTriangles(_source);
            _containers = Utils.Puzzle.InitContainers(_triangles);
            Utils.Puzzle.RandomMove(
                _containers,
                new Point(pbMain.Width / 3, pbMain.Height / 3),
                new Point(pbMain.Width / 3 * 2, pbMain.Height / 3 * 2));

            ChangeScale();
            _gameOn = true;
            pbMain.Invalidate();
        }

        #region Actions

        private void pbMain_Paint(object sender, PaintEventArgs e)
        {
            Draw(e);
        }

        private void pbMain_MouseDown(object sender, MouseEventArgs e)
        {
            StartMove(e);
        }

        private void pbMain_MouseUp(object sender, MouseEventArgs e)
        {
            StopMove();
        }


        private void pbMain_MouseMove(object sender, MouseEventArgs e)
        {
            MoveContainer(e);
        }

        private void tsLoadImage_Click(object sender, EventArgs e)
        {
            LoadImage();
        }

        private void stAutoSize_Click(object sender, EventArgs e)
        {
            _autoSize = !_autoSize;
            stAutoSize.Checked = _autoSize;
            ChangeScale();
            if (!_autoSize)
            {
                Scale(1);
            }
            pbMain.Invalidate();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ChangeScale();
        }
        #endregion

        private void LoadImage()
        {
            DialogResult result = ofdImage.ShowDialog();
            
            if (result == DialogResult.OK)
            {
                _filePath = ofdImage.FileName;
                Init();
                tsRestart.Enabled = true;
            }
        }

        private void tsRestart_Click(object sender, EventArgs e)
        {
            Init();
        }

        private void MoveContainer(MouseEventArgs e)
        {
            if (_move)
            {
                foreach (var triangle in _containers.Where(t => t.IsSelect)
                    .SelectMany(container => container.Triangles))
                {
                    triangle.Move(e.Location);
                }
                pbMain.Invalidate();
            }
        }

        private void StartMove(MouseEventArgs e)
        {
            if (_containers==null)
            {
                return;
            }
            _move = true;
            var selected = false;
            foreach (var container in _containers.OrderByDescending(c=>c.ZIndex))
            {
                foreach (var triangle in container.Triangles)
                {
                    if (!selected)
                    {
                        selected = triangle.IsSelected(e.Location);
                    }
                    triangle.MovePoint = new Point(triangle.Location.X - e.X, triangle.Location.Y - e.Y);
                }
                if (selected)
                {
                    container.ZIndex = _containers.Max(c => c.ZIndex)+1;
                    container.IsSelect = true;
                    CursorClip(true);
                    return;
                }
            }
        }

        private void StopMove()
        {
            CursorClip(false);
            _move = false;
            if (_containers == null)
            {
                return;
            }
            Compound();
            pbMain.Invalidate();
            foreach (var container in _containers)
            {
                container.IsSelect = false;
            }
            if (_gameOn)
            {
                IsVictory();
            }
        }

        private void Draw(PaintEventArgs e)
        {
            if (_source==null)
            {
                return;
            }

            foreach (var triangle in _containers.OrderBy(c=>c.ZIndex).SelectMany(container => container.Triangles))
            {
                e.Graphics.DrawImage(triangle.Image, triangle.Location);
            }
        }

        private void Compound()
        {
            Utils.Puzzle.Compound(_containers, _epsilon);
        }

        private void IsVictory()
        {
            if (Utils.Puzzle.IsVictory(_containers))
            {
                _gameOn = false;
                DialogResult dialogResult = MessageBox.Show(Properties.Resources.victory_again, Properties.Resources.victory, MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    this.Init();
                }
            }
        }

        private void ChangeScale()
        {
            if (_autoSize&& _source !=null)
            {
                var sizeScaleWidth = (double)pbMain.Width/2/_source.Width;
                var sizeScaleHeight = (double)pbMain.Height / 2/_source.Height;
                Scale(sizeScaleWidth> sizeScaleHeight? sizeScaleHeight: sizeScaleWidth);
            }
        }

        private void Scale(double scale)
        {
            //изменение размеров
            foreach (var triangle in _containers.SelectMany(container => container.Triangles))
            {
                triangle.Scale(scale, _curScale);
            }
            //скрепление соединенных деталей
            foreach (var container in _containers)
            {
                var bufContainer = new List<Container>();
                foreach (var triangle in container.Triangles)
                {
                    var c = new Container();
                    c.Triangles.Add(triangle);
                    bufContainer.Add(c);
                }
                while (bufContainer.Count>1)
                {
                    bufContainer.First().IsSelect = true;
                    Utils.Puzzle.Compound(bufContainer, int.MaxValue);
                }
            }
            _curScale = scale;
            pbMain.Invalidate();
        }

        private void CursorClip(bool on)
        {
            Cursor = new Cursor(Cursor.Current.Handle);
            Cursor.Clip = on ? new Rectangle(this.Location, this.Size) : Rectangle.Empty;
        }

    }
}
