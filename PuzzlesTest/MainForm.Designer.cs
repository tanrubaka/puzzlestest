﻿namespace PuzzlesTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbMain = new System.Windows.Forms.PictureBox();
            this.msMainMenu = new System.Windows.Forms.MenuStrip();
            this.tsMain = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLoadImage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsRestart = new System.Windows.Forms.ToolStripMenuItem();
            this.stSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.stAutoSize = new System.Windows.Forms.ToolStripMenuItem();
            this.ofdImage = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).BeginInit();
            this.msMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbMain
            // 
            this.pbMain.BackColor = System.Drawing.Color.Transparent;
            this.pbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbMain.Location = new System.Drawing.Point(0, 24);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(842, 599);
            this.pbMain.TabIndex = 0;
            this.pbMain.TabStop = false;
            this.pbMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pbMain_Paint);
            this.pbMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbMain_MouseDown);
            this.pbMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbMain_MouseMove);
            this.pbMain.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbMain_MouseUp);
            // 
            // msMainMenu
            // 
            this.msMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMain,
            this.stSettings});
            this.msMainMenu.Location = new System.Drawing.Point(0, 0);
            this.msMainMenu.Name = "msMainMenu";
            this.msMainMenu.Size = new System.Drawing.Size(842, 24);
            this.msMainMenu.TabIndex = 1;
            this.msMainMenu.Text = "Главное меню";
            // 
            // tsMain
            // 
            this.tsMain.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLoadImage,
            this.tsRestart});
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(53, 20);
            this.tsMain.Text = "Меню";
            // 
            // tsLoadImage
            // 
            this.tsLoadImage.Name = "tsLoadImage";
            this.tsLoadImage.Size = new System.Drawing.Size(181, 22);
            this.tsLoadImage.Text = "Загрузить картинку";
            this.tsLoadImage.Click += new System.EventHandler(this.tsLoadImage_Click);
            // 
            // tsRestart
            // 
            this.tsRestart.Enabled = false;
            this.tsRestart.Name = "tsRestart";
            this.tsRestart.Size = new System.Drawing.Size(181, 22);
            this.tsRestart.Text = "Начать заново";
            this.tsRestart.Click += new System.EventHandler(this.tsRestart_Click);
            // 
            // stSettings
            // 
            this.stSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stAutoSize});
            this.stSettings.Name = "stSettings";
            this.stSettings.Size = new System.Drawing.Size(79, 20);
            this.stSettings.Text = "Настройки";
            // 
            // stAutoSize
            // 
            this.stAutoSize.Name = "stAutoSize";
            this.stAutoSize.Size = new System.Drawing.Size(209, 22);
            this.stAutoSize.Text = "Автоматический размер";
            this.stAutoSize.Click += new System.EventHandler(this.stAutoSize_Click);
            // 
            // ofdImage
            // 
            this.ofdImage.Filter = "*.BMP; *.JPG; *.GIF; *.PNG)| *.BMP; *.JPG; *.GIF;*.PNG";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 623);
            this.Controls.Add(this.pbMain);
            this.Controls.Add(this.msMainMenu);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.msMainMenu;
            this.Name = "MainForm";
            this.Text = "Пазл";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pbMain)).EndInit();
            this.msMainMenu.ResumeLayout(false);
            this.msMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbMain;
        private System.Windows.Forms.MenuStrip msMainMenu;
        private System.Windows.Forms.ToolStripMenuItem tsMain;
        private System.Windows.Forms.ToolStripMenuItem tsLoadImage;
        private System.Windows.Forms.ToolStripMenuItem tsRestart;
        private System.Windows.Forms.OpenFileDialog ofdImage;
        private System.Windows.Forms.ToolStripMenuItem stSettings;
        private System.Windows.Forms.ToolStripMenuItem stAutoSize;
    }
}

