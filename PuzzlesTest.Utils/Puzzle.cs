﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzlesTest.Domains;

namespace PuzzlesTest.Utils
{
    public static class Puzzle
    {
        public static List<Triangle> InitTriangles(Bitmap source)
        {
            var pointsX = new int[5];
            var pointsY = new int[3];
            var width = (double)source.Width / 4;
            var height = (double)source.Height / 2;
            for (int i = 0; i < 5; i++)
            {
                pointsX[i] = (int)(i * width);
            }
            for (int i = 0; i < 3; i++)
            {
                pointsY[i] = (int)(i * height);
            }

            var triangles = new List<Triangle>();
            var invert = false;
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (invert)
                    {
                        var t = new Triangle(
                           new Point(pointsX[i], pointsY[j]),
                           new Point(pointsX[i + 1], pointsY[j + 1]),
                           new Point(pointsX[i], pointsY[j + 1]),
                           source
                           );
                        var t2 = new Triangle(
                            new Point(pointsX[i + 1], pointsY[j + 1]),
                            new Point(pointsX[i + 1], pointsY[j]),
                            new Point(pointsX[i], pointsY[j]),
                           source
                            );
                        triangles.Add(t);
                        triangles.Add(t2);
                    }
                    else
                    {
                        var t = new Triangle(
                           new Point(pointsX[i], pointsY[j]),
                           new Point(pointsX[i + 1], pointsY[j]),
                           new Point(pointsX[i], pointsY[j + 1]),
                           source
                           );
                        var t2 = new Triangle(
                            new Point(pointsX[i + 1], pointsY[j + 1]),
                            new Point(pointsX[i + 1], pointsY[j]),
                            new Point(pointsX[i], pointsY[j + 1]),
                           source
                            );
                        triangles.Add(t);
                        triangles.Add(t2);
                    }
                    invert = !invert;
                }
                invert = !invert;
            }
            //индекс склеивания ребра
            var edgeIndex = 1;
            for (int i = 0; i < triangles.Count; i++)
            {
                foreach (var edge in triangles[i].Edges)
                {
                    if (edge.Index != 0)
                    {
                        continue;
                    }
                    edge.Index = edgeIndex;
                    for (int j = i + 1; j < triangles.Count; j++)
                    {
                        var edges = triangles[j].Edges
                            .Where(e =>
                                (e.Start == edge.Start || e.Start == edge.End) &&
                                (e.End == edge.Start || e.End == edge.End)).ToList();
                        foreach (var e in edges)
                        {
                            e.Index = edge.Index;
                        }
                    }
                    edgeIndex++;
                }
            }
            return triangles;
        }

        public static List<Container> InitContainers(List<Triangle> triangles)
        {
            var zIndex = 0;
            return triangles.Select(triangle => new Container
            {
                ZIndex = ++zIndex,
                Triangles = new List<Triangle> {triangle}
            }).ToList();
        }

        public static void RandomMove(List<Container> containers, Point minPoint, Point maxPoint)
        {
            var rnd = new Random();
            foreach (var container in containers)
            {
                var e = container.Triangles.First().Edges.First();
                var movePoint = new Point(rnd.Next(minPoint.X,maxPoint.X), rnd.Next(minPoint.Y, maxPoint.Y));

                MoveContainerTo(container, movePoint, e.Start);
            }
        }

        public static void Compound(List<Container> sourceContainers, int epsilon)
        {
            var selectContainer = sourceContainers.SingleOrDefault(c => c.IsSelect);
            if (selectContainer == null)
            {
                return;
            }
            foreach (var triangle in selectContainer.Triangles.ToList())
            {
                foreach (var edge in triangle.Edges)
                {
                    var containers = sourceContainers.Where(c =>
                        c.Triangles
                            .Any(t => t.Edges
                                .Any(e => e.Index == edge.Index &&
                                    (
                                        (Math.Abs(e.Start.X - edge.Start.X) <= epsilon && Math.Abs(e.Start.Y - edge.Start.Y) <= epsilon) ||
                                        (Math.Abs(e.End.X - edge.Start.X) <= epsilon && Math.Abs(e.End.Y - edge.Start.Y) <= epsilon)
                                    ) &&
                                    (
                                        (Math.Abs(e.Start.X - edge.End.X) <= epsilon && Math.Abs(e.Start.Y - edge.End.Y) <= epsilon) ||
                                        (Math.Abs(e.End.X - edge.End.X) <= epsilon && Math.Abs(e.End.Y - edge.End.Y) <= epsilon)
                                    )
                                    )) &&
                        c != selectContainer).ToList();

                    foreach (var container in containers)
                    {
                        foreach (var t in container.Triangles)
                        {
                            var ee = t.Edges.SingleOrDefault(e => e.Index == edge.Index);
                            if (ee != null)
                            {
                                MoveContainerTo(
                                    selectContainer, 
                                    MainPoint(ee.Start, ee.End),
                                     MainPoint(edge.Start, edge.End));
                            }
                        }
                        foreach (var t in container.Triangles)
                        {
                            selectContainer.Triangles.Add(t);
                        }
                        sourceContainers.Remove(container);
                    }
                }
            }
        }

        private static Point MainPoint(Point start, Point end)
        {
            var main = new Point();
            if (start.Y < end.Y)
            {
                main = start;
            }
            else if (start.Y > end.Y)
            {
                main = end;
            }
            else if (start.X < end.X)
            {
                main = start;
            }
            else if (start.X > end.X)
            {
                main = end;
            }
            return main;
        }

        public static void MoveContainerTo(Container container, Point to, Point oldLocation)
        {
            foreach (var selectTriangle in container.Triangles)
            {
                selectTriangle.MovePoint = new Point(
                    selectTriangle.Location.X - oldLocation.X, 
                    selectTriangle.Location.Y - oldLocation.Y);
                selectTriangle.Move(to);
            }
        }

        public static bool IsVictory(List<Container> containers)
        {
            return containers.Count == 1;
        }
    }
}
