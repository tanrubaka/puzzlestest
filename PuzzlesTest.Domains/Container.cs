﻿using System.Collections.Generic;

namespace PuzzlesTest.Domains
{
    public class Container
    {
        public Container()
        {
            Triangles = new List<Triangle>();
        }
        public int ZIndex { get; set; }
        public bool IsSelect { get; set; }
        public List<Triangle> Triangles { get; set; } 
    }
}