﻿using System.Drawing;

namespace PuzzlesTest.Domains
{
    public class Edge
    {
        public Point Start { get; set; }
        public Point End { get; set; }
        public int Index { get; set; }
    }
}