﻿using System.Drawing;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;

namespace PuzzlesTest.Domains
{
    public class Triangle
    {
        public Triangle(Point vertex1, Point vertex2, Point vertex3, Bitmap image)
        {
            Vertices = new List<Point> { vertex1, vertex2, vertex3 };
            Edges = new List<Edge>();
            for (int i = 0; i < Vertices.Count; i++)
            {
                Edges.Add(new Edge
                {
                    Start = Vertices[i],
                    End = i + 1< Vertices.Count? Vertices[i+1]: Vertices[0]
                });
            }
            SetRegion();
            Location = new Point(Vertices.Min(i=>i.X), Vertices.Min(i => i.Y));
            OriginVertices = new List<Point>
            {
                new Point(vertex1.X - Location.X,vertex1.Y - Location.Y),
                new Point(vertex2.X - Location.X,vertex2.Y - Location.Y),
                new Point(vertex3.X - Location.X,vertex3.Y - Location.Y),
            };
            CreateBitmap(image);
        }
        public Bitmap Image { get; set; }
        public Bitmap OriginImage { get; set; }
        public List<Point> Vertices { get; set; }
        public List<Point> OriginVertices { get; set; }
        public List<Edge> Edges { get; set; }
        public GraphicsPath GraphicsPath { get; set; }
        public Region Region { get; set; }
        public Point Location { get; set; }
        public Point MovePoint { get; set; }

        private void SetRegion()
        {
            GraphicsPath = new GraphicsPath();
            GraphicsPath.AddPolygon(Vertices.ToArray());
            Region = new Region(GraphicsPath);
        }

        private void SetEdges()
        {
            for (int i = 0; i < Vertices.Count; i++)
            {
                Edges[i].Start = Vertices[i];
                Edges[i].End = i + 1 < Vertices.Count ? Vertices[i + 1] : Vertices[0];
            }
        }

        public bool IsSelected(Point mouse)
        {
            return Region.IsVisible(mouse);
        }

        public void Move(Point mouse)
        {
            var newLocation =  new Point(mouse.X + MovePoint.X, mouse.Y + MovePoint.Y);

            for (int i = 0; i < Vertices.Count; i++)
            {
                var v = Vertices[i];
                v.X = v.X - Location.X + newLocation.X;
                v.Y = v.Y - Location.Y + newLocation.Y;
                Vertices[i] = v;
            }
            SetEdges();
            SetRegion();
            Location = newLocation;
        }

        private void CreateBitmap(Bitmap source)
        {
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            
            //Determine the destination size/position
            x = source.Width;
            y = source.Height;

            foreach (var p in Vertices)
            {
                if (p.X < x)
                    x = p.X;
                if (p.X > width)
                    width = p.X;

                if (p.Y < y)
                    y = p.Y;
                if (p.Y > height)
                    height = p.Y;
            }

            height = height - y;
            width = width - x;


            
            Matrix m = new Matrix(1, 0, 0, 1, -x, -y);
            GraphicsPath.Transform(m);

            //Create the Bitmap
            OriginImage = new Bitmap(width, height);
            Image = OriginImage;
            //Draw on the Bitmap
            using (Graphics g = Graphics.FromImage(Image))
            {
                g.SetClip(GraphicsPath);
                g.DrawImage(source, -x, -y);
            }
        }

        public void Scale(double scale, double oldScale)
        {
            Location = new Point((int)(Location.X/ oldScale * scale), (int)(Location.Y/ oldScale * scale));
            for (int i = 0; i < Vertices.Count; i++)
            {
                var v = Vertices[i];
                v.X = (int)((OriginVertices[i].X) *scale) + Location.X;
                v.Y = (int)((OriginVertices[i].Y) * scale) + Location.Y;
                Vertices[i] = v;
            }
            SetEdges();
            SetRegion();
            Image = new Bitmap(OriginImage, (int)(OriginImage.Width*scale), (int)(OriginImage.Height * scale));
        }
    }
}
