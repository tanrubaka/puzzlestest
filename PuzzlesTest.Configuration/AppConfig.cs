﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzlesTest.Configuration
{
    public static class AppConfig
    {
        public static int Epsilon
        {
            get
            {
                int value;
                if (int.TryParse(ConfigurationManager.AppSettings["Epsilon"], out value))
                {
                    return value;
                }
                throw new InvalidOperationException(Resources.Resource.ErrorEpsilon);
            }
        }
    }
}
